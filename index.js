
// №1
const stopka = Array(100).fill(1).map((el,i) => i).reverse()
const podstavki = {
  '1': [stopka],
  '2': [],
  '3': []
}
console.log("initial state podstavok",podstavki)

const func = (stopka,numPodstavaka) => {
  const fillStopkaNow = Object.values(podstavki).findIndex(el => el.length > 0) + 1
  if (fillStopkaNow === numPodstavaka) {
    return podstavki
  }
  console.log("fillStopkaNow",fillStopkaNow)
  const freeSlots = [1,2,3].filter(el => el !== fillStopkaNow)
  console.log("freeSlots",freeSlots)
  const firstStepForStopka = freeSlots.filter(el => el !== numPodstavaka)
  console.log("firstStepForStopka",firstStepForStopka)
  const stateStopkaFirst = stopka.slice().reverse()
  const firstStep = {
    1:[],
    2:[],
    3:[],
    [firstStepForStopka]: stateStopkaFirst
  }
  const stateStopkaSecond = stateStopkaFirst.slice().reverse()
  console.log("firstStep",firstStep)
  const secondStep = {
    1:[],
    2:[],
    3:[],
    [numPodstavaka]: stateStopkaSecond
  }
  return secondStep
}

console.log(func(stopka,3))


